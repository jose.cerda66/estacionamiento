﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace Estacionamiento
{
    public partial class Reportes : Form
    {
        public Reportes()
        {
            InitializeComponent();
            string server = "localhost";
            string puerto = "3306";
            string user = "root";
            string pwd = "";
            string database = "estacionamiento_prueba";
            string consulta = "SELECT id_parking, ticket FROM estacionamiento";
            string connStr = "server=" + server + ";uid=" + user + ";pwd=" + pwd + ";database=" + database + ";port=" + puerto;
            try
            {

                MySqlConnection conexion = new MySqlConnection(connStr);
                conexion.Open();

                MySqlDataAdapter adaptador = new MySqlDataAdapter();
                adaptador.SelectCommand = new MySqlCommand(consulta, conexion);
                DataTable tabla = new DataTable();
                adaptador.Fill(tabla);

                /*Bindear los datos con la tabla y el datasource la fuente de datos a la tabla*/
                BindingSource bSource = new BindingSource();
                bSource.DataSource = tabla;
                /*el datagriview1 mas el datasource es igual al binding source*/
                dataGridView1.DataSource = bSource;
                conexion.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al conectar al servidor de MySQL: " +
                    ex.Message, "Error al conectar",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Reportes_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string server = "localhost";
            string puerto = "3306";
            string user = "root";
            string pwd = "";
            string database = "estacionamiento_prueba";
            string consulta = "SELECT id_parking, ticket FROM estacionamiento WHERE (ticket='abierto')";
            string connStr = "server=" + server + ";uid=" + user + ";pwd=" + pwd + ";database=" + database + ";port=" + puerto;
            try
            {

                MySqlConnection conexion = new MySqlConnection(connStr);
                conexion.Open();

                MySqlDataAdapter adaptador = new MySqlDataAdapter();
                adaptador.SelectCommand = new MySqlCommand(consulta, conexion);
                DataTable tabla = new DataTable();
                adaptador.Fill(tabla);

                /*Bindear los datos con la tabla y el datasource la fuente de datos a la tabla*/
                BindingSource bSource = new BindingSource();
                bSource.DataSource = tabla;
                /*el datagriview1 mas el datasource es igual al binding source*/
                dataGridView1.DataSource = bSource;
                conexion.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al conectar al servidor de MySQL: " +
                    ex.Message, "Error al conectar",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string server = "localhost";
            string puerto = "3306";
            string user = "root";
            string pwd = "";
            string database = "estacionamiento_prueba";
            string consulta = "SELECT id_parking, ticket FROM estacionamiento WHERE (ticket='cerrado')";
            string connStr = "server=" + server + ";uid=" + user + ";pwd=" + pwd + ";database=" + database + ";port=" + puerto;
            try
            {

                MySqlConnection conexion = new MySqlConnection(connStr);
                conexion.Open();

                MySqlDataAdapter adaptador = new MySqlDataAdapter();
                adaptador.SelectCommand = new MySqlCommand(consulta, conexion);
                DataTable tabla = new DataTable();
                adaptador.Fill(tabla);

                /*Bindear los datos con la tabla y el datasource la fuente de datos a la tabla*/
                BindingSource bSource = new BindingSource();
                bSource.DataSource = tabla;
                /*el datagriview1 mas el datasource es igual al binding source*/
                dataGridView1.DataSource = bSource;
                conexion.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al conectar al servidor de MySQL: " +
                    ex.Message, "Error al conectar",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string server = "localhost";
            string puerto = "3306";
            string user = "root";
            string pwd = "";
            string database = "estacionamiento_prueba";
            string consulta = "SELECT id_parking, ticket FROM estacionamiento";
            string connStr = "server=" + server + ";uid=" + user + ";pwd=" + pwd + ";database=" + database + ";port=" + puerto;
            try
            {

                MySqlConnection conexion = new MySqlConnection(connStr);
                conexion.Open();

                MySqlDataAdapter adaptador = new MySqlDataAdapter();
                adaptador.SelectCommand = new MySqlCommand(consulta, conexion);
                DataTable tabla = new DataTable();
                adaptador.Fill(tabla);

                /*Bindear los datos con la tabla y el datasource la fuente de datos a la tabla*/
                BindingSource bSource = new BindingSource();
                bSource.DataSource = tabla;
                /*el datagriview1 mas el datasource es igual al binding source*/
                dataGridView1.DataSource = bSource;
                conexion.Close();
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al conectar al servidor de MySQL: " +
                    ex.Message, "Error al conectar",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string server = "localhost";
            string puerto = "3306";
            string user = "root";
            string pwd = "";
            string database = "estacionamiento_prueba";
            string consulta = "SELECT SUM(monto_total) FROM caja";
            string connStr = "server=" + server + ";uid=" + user + ";pwd=" + pwd + ";database=" + database + ";port=" + puerto;
            try
            {

                MySqlConnection conexion = new MySqlConnection(connStr);
                conexion.Open();
                MySqlCommand cmd = new MySqlCommand(consulta, conexion);

                MySqlDataReader lector = cmd.ExecuteReader();

               

                if (lector.Read())
                {
                    textBox1.Text = lector[0].ToString();
                }

                }
            catch (MySqlException ex) {
                MessageBox.Show("Error al obtener los datos"+ ex);
            }
    }

        private void button5_Click(object sender, EventArgs e)
        {
            Menu ventana = new Menu();
            this.Close();
            ventana.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
